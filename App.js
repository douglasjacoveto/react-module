import React, { Component } from 'react';
import { AppRegistry, Button, FlatList, StyleSheet, Text, View } from 'react-native';

export default class FlatListBasics extends Component {
  render() {
    return (

      <View style={styles.container}>
        <FlatList
          data={[
            { key: 'Jan' },
            { key: 'Fev' },
            { key: 'Mar' },
            { key: 'Abr' },
            { key: 'Mai' },
            { key: 'Jun' },
            { key: 'Jul' },
            { key: 'Ago' },
            { key: 'Set' },
            { key: 'Out' },
            { key: 'Nov' },
            { key: 'Dez' },
          ]}
          renderItem={({ item }) => <Text style={styles.item}>{item.key}</Text>}
        />
        <Button
          onPress={() => {
            alert('You tapped the button!');
          }}
          title="Press Me" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})

// //Create a Component
// const App = () => (
//   <HelloWorld />
// );

// //Export App - This line solved my issue
// export default App;

AppRegistry.registerComponent('hello_world', () => HelloWorld);